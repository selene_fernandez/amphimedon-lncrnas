%#####################################
%# Code by: Itai Yanai 
%#####################################
%# 
%# The MIT License
%# 
%# Copyright (c) 2014 Itai Yanai 
%# 
%# Permission is hereby granted, free of charge, to any person obtaining a copy
%# of this software and associated documentation files (the "Software"), to deal
%# in the Software without restriction, including without limitation the rights
%# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%# copies of the Software, and to permit persons to whom the Software is
%# furnished to do so, subject to the following conditions:
%# 
%# The above copyright notice and this permission notice shall be included in
%# all copies or substantial portions of the Software.
%# 
%# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%# THE SOFTWARE.
%# 
%####################################
%# If you use this code please cite: 
i%#
%# Dynamic and widespread lncRNA expression in the sponge and the origin of animal complexity. Federico Gaiti, Selene L. Fernandez-Valverde, Nagayasu Nakanishi, Itai Yanai, Milos Tanurdzic and Bernard M. Degnan Submitted
%####################################

initial_analysis = 0;
second_analysis  = 1;

if (initial_analysis)
    figure;
    A=dlmread('TCAST_CEL-Seq_lncRNAs.txt');
    read_thresh_p = [10 20 50 100];
    
    for read_thresh = 1:length(read_thresh_p)
        % filter
        i = find(sum(A')>=read_thresh_p(read_thresh)); B = A(i,:);
        
        %smooth
        for i=1:length(B)
            B(i,:) = smooth(B(i,:),5);
        end
        
        %standardize
        C = (B -repmat(mean(B')',1,size(B,2)))./repmat(std(B')',1,size(B,2)); %standardize
        
        %sort
        subplot(1,length(read_thresh_p), read_thresh);
        [i,j,k] = dendrogram(linkage(pdist(C)),0); %k = 1:length(k)
        imagesc(C(k,:)); title(read_thresh_p(read_thresh))
    end
    load cmap_color_blind; colormap(cmap_color_blind)
end

if (second_analysis)
    figure;
    try
        A; A_coding;
    catch
        A=dlmread('TCAST_CEL-Seq_lncRNAs.txt');
        A_coding=dlmread('TCAST_CEL-Seq_CodingGenes.txt');
    end
    
    read_thresh_p = [50];
    i = find(sum(A')>=read_thresh_p); B_lnc = A(i,:);
    
    read_thresh_p = [1000];
    i = find(sum(A_coding')>=read_thresh_p); B_coding = A_coding(i,:);
    
    B = [B_lnc;B_coding];
    type = zeros(1,length(B)); type(1:size(B_lnc,1))=1;
    
    %smooth
    for i=1:length(B)
        B(i,:) = smooth(B(i,:),5);
    end
    
    %standardize
    C = (B -repmat(mean(B')',1,size(B,2)))./repmat(std(B')',1,size(B,2)); %standardize
    
    %sort
    try
        load k_save;
        k;
    catch
        [i,j,k] = dendrogram(linkage(pdist(C),'centroid'),0); %k = 1:length(k)
    end
    
    k=k([1624:3218, 1623:-1:1]);
    subplot('position',[0.05 0.1 0.4 0.8])
    imagesc(C(k,:));     load cmap_color_blind; colormap(cmap_color_blind)
    
    subplot('position',[0.55 0.1 0.4 0.8])
    imagesc(corrcoef(C(k,:)'));     load cmap_color_blind; colormap(cmap_color_blind)
    set(gca,'yticklabel','');
    
    subplot('position',[0.47 0.1 0.075 0.8])
    imagesc(smooth(type(k),200) , [0 0.1]); set(gca,'xticklabel',''); set(gca,'yticklabel','')
    %     subplot('position',[0.1 0.1 0.1 0.8])
    %     [~,~,c] = intersect(1:sum(type),k);
    %     factor = length(k)/sum(type);
    %     locations = 1:factor:length(k);
    %     for i = 1:sum(type)
    %
    %         plot(1,locations(i),'ko'); hold on;
    %         line([1,2],[locations(i) c(i)],'color','k');
    %     end
    % %     imagesc(C(k,:)); title(read_thresh_p(read_thresh))
    %
end
